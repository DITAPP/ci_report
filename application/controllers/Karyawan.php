<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("karyawan_model");
		$this->load->model("jabatan_model");

		$this->load->library('form_validation');

		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("user/index", "refresh");
		}

	}
	
	public function index()
	{
		$this->listkaryawan();
	}
	
	public function listkaryawan()
	{
		//proses pencarian data
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		} else {
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
		}

		$data['data_karyawan'] = $this->karyawan_model->tombolpagination($data['kata_pencarian']);
		//$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['content']	   = 'forms/list_karyawan';
		$this->load->view('home_', $data);
	}
	
	public function inputkaryawan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();

		//if(!empty($_REQUEST)) {
		//	$m_karyawan = $this->karyawan_model;
		//	$m_karyawan->save();
		//	redirect("karyawan/index", "refresh");
		//	}
		
		//validasi terlebih dahulu
		$validation = $this->form_validation;
		$validation->set_rules($this->karyawan_model->rules());
		
		if ($validation->run()){
			$this->karyawan_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil</div>');
			redirect("karyawan/index", "refresh");
			}
			
		$data['content'] ='forms/input_karyawan';
		$this->load->view('home_', $data);
	}
	
	public function detailkaryawan($nik)
	{
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		$this->load->view('detail_karyawan', $data);
	}
	
	public function editkaryawan($nik)
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['edit_karyawan'] = $this->karyawan_model->tampilDataKaryawan($nik);
		
		//if(!empty($_REQUEST)) {
		//	$m_karyawan = $this->karyawan_model;
		//	$m_karyawan->update($nik);
		//	redirect("karyawan/index", "refresh");
		//	}
		
		$validation = $this->form_validation;
		$validation->set_rules($this->karyawan_model->rules());
		
		if ($validation->run()){
			$this->karyawan_model->update($nik);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil</div>');
			redirect("karyawan/index", "refresh");
			}
			
		$data['content'] ='forms/edit_karyawan';
		$this->load->view('home_', $data);	
	}
	
	public function deletekaryawan($nik)
	{
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->delete($nik);
			redirect("karyawan/index", "refresh");
	}
	
}