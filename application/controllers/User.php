<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class User extends CI_Controller{
	
	public function __construct()
	{
	parent:: __construct();
	$this->load->model("User_model");

	}
	public function index()
	{
	$user_login	=$this->session->userdata();

	if(count($user_login)<=1) {
			$this->login();
		} else {redirect("home/");}


	}
	public function login()
	{
		if (!empty($_REQUEST))
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$data_login = $this->User_model->CekUser($username, $password);



			$data_sesi = array(
				'username' => $data_login['nik'],
				'email'	   => $data_login['email'],
				'tipe'	   => $data_login['tipe'],
				'status'   => "login"
			);
		if (!empty($data_login)) {
			$this->session->set_userdata($data_sesi);
			redirect("home/", "refresh");
		} else {
			$this->session->set_flashdata('info', 'Username Atau Password Salah!');
			redirect("user/", "refresh");
		}
	}
	$this->load->view('login');

	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect("user/","refresh");
	}
}