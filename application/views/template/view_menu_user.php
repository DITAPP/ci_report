<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style.css">
<div class="menu">
    <ul>
    <li><a href="<?=base_url();?>karyawan/listkaryawan">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
    		<li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
    		<li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Data Jenis Barang</a></li>
    		<li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
            <li><a href="<?=base_url();?>user/logout">Logout</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="<?=base_url();?>pembelian/input_h">Pembelian</a></li>
            <li><a href="#">Penjualan</a></li>
        </ul>
    </li>
    <li><a href="#">Report</a></li>
    <li><a href="<?=base_url();?>user/logout"">Log ut</a></li>
    </ul>
    </div>