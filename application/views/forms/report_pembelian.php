<div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>LAPORAN DATA PEMBELIAN </b><br>
                </div>
            
    
    <ul>
    <h4 align="left">
    <a href="<?=base_url();?>pembelian/input_h">Laporan Data Pembelian Dari Tanggal 2019-02-01 S/D 2019-04-23</a></h4>
    </ul>
     <tr align="center">
    <td width="50%" style="text-align: left; font-color: green;">
    <?php
    	if($this->session->flashdata('info') == true){
			echo $this->session->flashdata('info');
			}
	?>
	</td>
    <table width="100%" border="1">
      <tr align="center" bgcolor="#CCCCCC">
        <td>No</td>
        <td>Id Pembelian</td>
        <td>No Transaksi</td>
        <td>Tanggal</td>
        <td>Total Barang</td>
        <td>Total Qty</td>
        <td>Jumlah Nominal Pembelian</td>
      </tr>
<?php
	$no = 0;
	$total  = 0;
	foreach ($data_pembelian as $data)
	{
	$no++;
?>
      <tr align="center">
        <td><?=$no;?></td>
        <td><?= $data->id_pembelian_h; ?></td>
        <td><?= $data->no_transaksi; ?></td>
        <td><?= $data->tanggal; ?></td>
        <td><?= $data->stok; ?></td>
        <td><?= $data->qty; ?></td>
        <td align="right">Rp. <?= number_format($data->jumlah); ?> ,-</td>
        <td>
        <a href="<?=base_url(); ?>pembelian/report"><input type="button" name="<<kembali>>" id="<<kembali>>"
        value="<<kembali>>"></a>
        </td>
      </tr>
  <?php
  	// hitung total
	$total += $data->jumlah;
	}
  ?>
  <tr align="center" bgcolor="yellow">
  <td colspan="6" align="center"><b>TOTAL KESELURUHAN PEMBELIAN</b></td>
  <td align="right">Rp. <b><?= number_format($total); ?></b></td>
  </tr>
</table>